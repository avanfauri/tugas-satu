package edu.stts.tugasandroid1

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import kotlinx.android.synthetic.main.fragment_login_bottom.*

class LoginBottomFragment : Fragment() {

    companion object {
        fun newInstance() = LoginBottomFragment()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Get the custom view for this fragment layout
        val view = inflater!!.inflate(R.layout.fragment_login_bottom,container,false)

        // Return the fragment view/layout
        return view
    }

    fun receiveUser(user: User) {

        if (user.username.equals("") || user.password.equals("")) {

            txt_status.append(getString(R.string.empty_username_password)+System.getProperty("line.separator"))

        } else {

            if (!user.password.equals("stts")) {

                txt_status.append(getString(R.string.password_must_be_stts)+System.getProperty("line.separator"))

            } else {

                Toast.makeText(this.context,user.username, Toast.LENGTH_SHORT).show()

                // go to the main activity
                val i = MainActivity.getStartIntent(
                        requireContext(),
                        User(
                                user.username,
                                user.password
                        )
                )

                startActivity(i)

            }

        }

    }

}